/**
 * 
 */

/**
 * @author alfrex
 *
 */
public class Singleton {
private static Singleton objeto=null;
private Singleton(){}
public static Singleton getSingleton(){
if(objeto==null){
System.out.println("me llaman");
objeto=new Singleton();
}
return objeto;
}
public void ver(){
System.out.println("llamando desde "+this.getClass().getName());
}
public int suma(int a, int b){
return a + b;
}
public int resta(int a, int b){
return a - b;
}
public int multiplicacion(int a, int b){
return a * b;
}
public int divicion(int a, int b){
return a / b;
}

/**
* @param args
*/
public static void main(String[] args) {
// TODO Auto-generated method stub
Singleton sig=Singleton.getSingleton();
Singleton sig1=Singleton.getSingleton();
Singleton sig2=Singleton.getSingleton();
Singleton sig3=Singleton.getSingleton();
System.out.println("operaciones basicas con: 6 y 3");
System.out.println("suma: "+ sig.suma(6, 3));
System.out.println("resta: "+ sig1.resta(6, 3));
System.out.println("multiplicacion: "+ sig2.multiplicacion(6, 3));
System.out.println("divicion: "+sig3.divicion(6, 3));

}

}
